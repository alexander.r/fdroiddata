Categories:Office
License:GPLv3
Web Site:http://www.droidbeard.com/
Source Code:https://github.com/rastating/DroidBeard
Issue Tracker:https://github.com/rastating/DroidBeard/issues

Auto Name:DroidBeard
Summary:Manage SickBeard installations
Description:
Managing your [http://sickbeard.com/ SickBeard] installation either at home or remotely.
.

Repo Type:git
Repo:https://github.com/rastating/DroidBeard

Build:1.2,1200
    commit=4892df7c63e22fd676001d19706fe578f2871e0c
    subdir=app
    gradle=yes

Build:1.3.4,1304
    commit=v1.3.4
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.3.4
Current Version Code:1304

